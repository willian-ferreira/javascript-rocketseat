/**
 * Crie uma função que dado o objeto a seguir:
 */

var endereco = {
  rua: 'Rua dos pinheiros',
  numero: 1293,
  bairro: 'Centro',
  cidade: 'São Paulo',
  uf: 'SP'
};

/**
 * Retorne o seguinte conteúdo:
 * O usuário mora em São Paulo / SP, no bairro Centro, na rua "Rua dos Pinheiros" com nº 1293.
 * @param {*} data 
 */
function mostraInfos(data) {
  return `O usuário mora em ${data.cidade} / ${data.uf}, no bairro ${data.bairro}, na rua ${data.rua} com nº ${data.numero}.`
}

console.log(mostraInfos(endereco));

/*
 *	Crie uma função que dado um intervalo (entre x e y) exiba todos número pares:
 */
function pares(x, y) {
  while (x < y) {
    x++;
    if (x % 2 == 0) {
      console.log(x);
    }
  }
}

/*
 *	Escreva uma função que verifique se o vetor de habilidades passado possui a habilidade "Javascript" e 
 *	retorna um booleano true/false caso exista ou não.
 */
function temHabilidade(skills) {
  return skills.includes('Javascript');
}

// Com for => como ensinado
function temHabilidade(skills) {
  for (var i = 0; i < skills.length; i++) {
    if (skills[i] == 'Javascript') {
      skills = true;
      console.log(skills);
    } else {
      skills = false;
      console.log(skills);
    }
  }
}


let skills = ['ReactJS', 'React Native', 'Javascript'];
temHabilidade(skills);

/*
 *	Escreva uma função que dado um total de anos de estudo retorna o quão experiente o usuário é:
 */
function experiencia(anosEstudo) {
  if (anosEstudo <= 1) console.log('Iniciante');
  else if (anosEstudo >= 1 && anosEstudo <= 3) console.log('Intermediário');
  else if (anosEstudo >= 3 && anosEstudo <= 6) console.log('Avançado');
  else if (anosEstudo >= 7) console.log('Jedi Master');
}
experiencia(1);
experiencia(4);
experiencia(10);
// De 0-1 ano: Iniciante
// De 1-3 anos: Intermediário
// De 3-6 anos: Avançado
// De 7 acima: Jedi Master

/**
 * Dado o seguinte vetor de objetos:
 */
var usuarios = [
  {
    nome: 'Diego',
    habilidades: ['Javascript', 'ReactJS', 'Redux']
  },
  {
    nome: 'Gabriel',
    habilidades: ['VueJS', 'Ruby on Rails', 'Elixir']
  }
]; 
/*
 *	Escreva uma função que produza o seguinte resultado:
 *
 * 	O Diego possui as habilidades: Javascript, ReactJS, Redux
 * 	O Gabriel possui as habilidades: VueJS, Ruby on Rails, Elixir
*/
function mostrarHabilidades(usuarios) {
  for (usuario of usuarios) {
    return 'O ' + usuario.nome + ' possui as habilidades: ' + usuario.habilidades.join(', ');
  }
}

mostrarHabilidades(usuarios);

/**
 * Em uma página crie uma função que dado um número de minutos crie um countdown em tempo 
 * real(de segundo a segundo) até chegar em zero no formato```08:30:20```, por exemplo:
 */

function countdown(minutos) {
  let data = 
  console.log(data)
}